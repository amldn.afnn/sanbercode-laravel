@extends('layout.templates')

@section('judul')
    Halaman Biodata
@endsection

@section('content')
<form action="/send" method="post">
    @csrf
    <label>
        First Name:
    </label><br>
    <input type="text" name="nameFirst"><br><br>
    <label>
        Last Name:
    </label><br>
    <input type="text" name="nameLast"><br><br>
    <label>
        Gender:
    </label><br>
    <input type="radio" value="Male" name="jenis kelamin">Male<br>
    <input type="radio" value="Female" name="jenis kelamin">Female<br>
    <input type="radio" value="Other" name="jenis kelamin">Other<br><br>
    <label>
        Nationality:
    </label><br>
    <select name="Nationality">
        <option value="indonesia">Indonesia</option>
        <option value="Paris">Paris</option>
        <option value="German">German</option>
    </select><br><br>
    <label>
        Language Spoken:
    </label><br>
    <input type="checkbox" name="language" value="indonesia language">Bahasa Indonesia<br>
    <input type="checkbox" name="language" value="paris language">Paris<br>
    <input type="checkbox" name="language" value="german language">German<br><br>
    <label>
        Bio:
    </label><br>
    <textarea name="bio" rows="10" cols="30"></textarea><br>
    <a href="/home"><input type="submit" value="send"></a>

</form>
@endsection
    
    