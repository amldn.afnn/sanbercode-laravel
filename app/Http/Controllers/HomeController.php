<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function regisForm()
    {
        return view('form.formRegis');
    }
    public function welcomePage()
    {
        return view('welcome');
    }
    public function homePage()
    {
        return view('page.home');
    }
    public function sendPage(Request $request)
    {
        $firstName = $request['nameFirst'];
        $lastName = $request['nameLast'];
        $gender = $request['jenis kelamin'];
        $nationality = $request['Nationality'];
        $language = $request['language'];
        $bio = $request['bio'];

        return view('page.home', ['firstName' => $firstName, 'lastName' => $lastName, 'gender' => $gender, 'nationality' => $nationality, 'language' => $language, 'bio' => $bio]);
    }
}
