<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'welcomePage']);
Route::get('/regisForm', [HomeController::class, 'regisForm']);
Route::get('/home', [HomeController::class, 'homePage']);
Route::post('/send', [HomeController::class, 'sendPage']);

Route::get('/data-table', function () {
    return view('page.datatable');
});

Route::get('/table', function () {
    return view('page.table');
});
